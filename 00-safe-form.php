<?php
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
//header('Content-type:application/json;charset=utf-8');
//print_r($str_json);
//print_r($response[0]);
//print_r($response[0]["value"]);
$longitud = count($response);
$valoresForm=array();
for($i=0;$i<$longitud;$i++){
	$valoresForm[$response[$i]["name"]] = $response[$i]["value"];
}
$Token = $valoresForm["token"];
$NombreCompleto = $valoresForm["nombre"];
$telefono = $valoresForm["telefono"];
$email = $valoresForm["mail"];
$idModalidad = $valoresForm["modalidad"];
$idNivel = $valoresForm["nivel"];
$idPlantel = $valoresForm["plantel"];
$idPrograma = $valoresForm["programa"];
$idPeriodo = $valoresForm["idPeriodo"];
$utm_source = $valoresForm["utm_source"];
$utm_medium = $valoresForm["utm_medium"];
$utm_campaign = $valoresForm["utm_campaign"];
$utm_content = $valoresForm["utm_content"];
$subOrigenCandidato = $valoresForm["subOrigenCandidato"];
$website = $valoresForm["website"];
$avisoPrivacidad = $valoresForm["avisoPriv"];
//CALCULADORA
if($subOrigenCandidato =="Calcula tu colegiatura"){
	$tipoBeca = $valoresForm["tipoBeca"];
	$promedio = $valoresForm["promedio"];
	$letraBeca = $valoresForm["letraBeca"];
	$idBeca = $valoresForm["idBeca"];
	$porcentajeDescuento = $valoresForm["descuentoInscripcion"];
	$porcentajeBeca = $valoresForm["porcentajeDescuento"];
}
if($avisoPrivacidad=="on"){
	$avisoPrivacidad = "true";
}else{
	$avisoPrivacidad = "false";
}
//SEPARAR NOMBRE Y APELLIDOS
class NombreSeparados{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
}
function separarNombre($nombreCompleto){
    $partsNombre = new NombreSeparados();    
	/* separar el nombre completo en espacios */
  $tokens = explode(' ', trim($nombreCompleto));
  /* arreglo donde se guardan las "palabras" del nombre */
  $names = array();
  /* palabras de apellidos (y nombres) compuetos */
  $special_tokens = array('da', 'de', 'del', 'la', 'las', 'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa');
  $prev = "";
  foreach($tokens as $token) { $_token = strtolower($token);
      if(in_array($_token, $special_tokens)){$prev .= "$token ";}else{ $names[] = $prev. $token; $prev = "";}
  }
  $num_nombres = count($names);
  $nombres = $apellidop = $apellidom = "";
	switch ($num_nombres) {
		case 0:
			$nombres = '';
			break;
		case 1: 
			$nombres = $names[0];
			break;
		case 2:
			$nombres    = $names[0];
			$apellidop  = $names[1];
			break;
		case 3:
			$nombres = $names[0];
			$apellidop   = $names[1];
			$apellidom   = $names[2];
			break;
		case 4:
			$nombres = $names[0].' '.$names[1];
			$apellidop   = $names[2];
			$apellidom   = $names[3];
			break;
		default:
			$nombres = $names[0] . ' ' . $names[1];
			unset($names[0]);
			unset($names[1]);
			$apellidop = implode(' ', $names);
			break;
	}
		$partsNombre->nombre = $nombres;
		$partsNombre->apellidoPaterno=$apellidop;
		$partsNombre->apellidoMaterno=$apellidom;
		return $partsNombre;
}

$separarNombre = separarNombre($NombreCompleto);
$nombre = $separarNombre->nombre;
$apellidoPaterno = $separarNombre->apellidoPaterno;
$apellidoMaterno = $separarNombre->apellidoMaterno;
//SEPARAR NOMBRE Y APELLIDOS
//SWITCH MODALIDADES
switch ($idModalidad){
	case "a031500000YMu7AAAT"://PRESENCIAL
		$modalidadEjecutiva="false";
		$modalidadOnline="false";
		$modalidadPresencial="true";
	break;
	case "a031500000YMu7CAAT"://ONLINE
		$modalidadEjecutiva="false";
		$modalidadOnline="true";
		$modalidadPresencial="false";
	break;
	case "a031500000YMu7DAAT"://EJECUTIVA
		$modalidadEjecutiva="true";
		$modalidadOnline="false";
		$modalidadPresencial="false";
	break;
	case "a036C000001ty9tQAA"://PRESENCIAL
		$modalidadEjecutiva="false";
		$modalidadOnline="false";
		$modalidadPresencial="true";
	break;
	case "a036C000001ty9vQAA"://ONLINE
		$modalidadEjecutiva="false";
		$modalidadOnline="true";
		$modalidadPresencial="false";
	break;
	case "a036C000001ty9wQAA"://EJECUTIVA
		$modalidadEjecutiva="true";
		$modalidadOnline="false";
		$modalidadPresencial="false";
	break;
	default:
		$modalidadEjecutiva="false";
		$modalidadOnline="false";
		$modalidadPresencial="true";
	break;		
}
//SWITCH MODALIDADES
$sendValues='
 "opcion" : "InsertaLead",  
 "Nombre": "'.$nombre.'",
 "apellidoPaterno": "'.$apellidoPaterno.'",
 "apellidoMaterno": "'.$apellidoMaterno.'",
 "status" : "Lead", 
 "telefono" : "'.$telefono.'", 
 "email": "'.$email.'",
 "idModalidad":"'.$idModalidad.'", 
 "idNivel": "'.$idNivel.'", 
 "idPlantel": "'.$idPlantel.'",
 "idPrograma": "'.$idPrograma.'",
 "idPeriodo": "'.$idPeriodo.'",
 "avisoPrivacidad": '.$avisoPrivacidad.',
 "asignacionAutomatica": true,
 "leadSource": "Marketing",
 "website": "'.$website.'",
 "subOrigenCandidato": "'.$subOrigenCandidato.'",
 "modalidadEjecutiva": '.$modalidadEjecutiva.',
 "modalidadOnline": '.$modalidadOnline.',
 "modalidadPresencial": '.$modalidadPresencial.',
 "utmSource": "'.$utm_source.'",
 "utmMedium": "'.$utm_medium.'",
 "utmCampaign": "'.$utm_campaign.'",
 "utmContent": "'.$utm_content.'"
';
if($subOrigenCandidato == "Calcula tu colegiatura"){
	$vigenciaDescuento=Date('Y-m-d', strtotime('+10 days'));
	$sendValues.=',
		"porcentajeDescuento": '.$porcentajeDescuento.',
		"porcentajeBeca": "'.$porcentajeBeca.'",
		"tipoBeca": "'.$tipoBeca.'",
		"promedio": "'.$promedio.'",
		"letraBeca": "'.$letraBeca.'",
		"idBeca": "'.$idBeca.'",
		"vigenciaDescuento": "'.$vigenciaDescuento.'"
	';
}
//print_r($sendValues);
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/manageLead',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{'.$sendValues.'}',
  CURLOPT_HTTPHEADER => array(
    'Username: oscar.tapia@ieu.edu.mx',
    'Authorization: Bearer '.$Token.'',
    'Content-Type: application/json',
    'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
  ),
));


$response = curl_exec($curl);
curl_close($curl);
echo $response;
//nuevo
//echo '{"idLead": "00Q6C00000OFxZqUAL","Mensaje": "Guardado"}';
//Duplicados
//echo '{"idLead": null,"Mensaje": "Duplicados","idLeadExistente": "00Q6C00000OFxN2UAL"}';

?>
