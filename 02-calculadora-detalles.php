<?php
	if(isset($_GET['idLead']) AND !empty($_GET['idLead'])){
		$idLead=$_GET['idLead'];
		$statuslead = $_GET['statuslead'];
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://dev03-ieu.cs63.force.com/services/apexrest/generaToken',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{
			"user" : "oscar.tapia@ieu.edu.mx",
			"password" : "852741"
		}',
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Cookie: BrowserId=HWAS4ZiQEeudajncMgaAqg'
		  ),
		));

		$responseToken = curl_exec($curl);
		// Comprueba el código de estado HTTP
		if (!curl_errno($curl)) {
			switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
				case 200:  # OK
					$items = json_decode($responseToken, true);
					$access_token = $items['access_token'];
				break;
				default:
					header('Location: 02-calculadora.php');
					echo 'Unexpected HTTP code: ', $http_code, "\n";
			}
		}
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/manageLead',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{
			"opcion" : "consultarLead",
			"idLead" : "'.$idLead.'"
		}',
		  CURLOPT_HTTPHEADER => array(
			'Username: oscar.tapia@ieu.edu.mx',
			'Authorization: Bearer '.$access_token.'',
			'Content-Type: application/json',
			'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
		  ),
		));

		$ConsultarLead = curl_exec($curl);
		if (!curl_errno($curl)) {
			switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
				case 200:  # OK
					$ArrayConsultarLead = json_decode($ConsultarLead, true);
					$leadEncontrado=true;
				break;
				default:
					$leadEncontrado=false;
					echo 'Unexpected HTTP code: ', $http_code, "\n";
			}
		}
		curl_close($curl);
	}else{
		header('Location: 02-calculadora.php');
	}
//print_r($ArrayConsultarLead);
	if($leadEncontrado==true){
		$idLead = $ArrayConsultarLead['Lead']['idLead'];
		$nombre = $ArrayConsultarLead['Lead']['nombre'];
		$apellidoPaterno = $ArrayConsultarLead['Lead']['apellidoPaterno'];
		$nombreCompleto = $nombre.' '.$apellidoPaterno;
		if (array_key_exists('apellidoMaterno', $ArrayConsultarLead['Lead'])) {
			$apellidoMaterno = $ArrayConsultarLead['Lead']['apellidoMaterno'];
			$nombreCompleto .= ' '.$apellidoMaterno;
		}
		$telefono = $ArrayConsultarLead['Lead']['telefono'];
		$email = $ArrayConsultarLead['Lead']['email'];
		
		$folioCalculadora = $ArrayConsultarLead['Lead']['folioCalculadora'];
		$folio = $ArrayConsultarLead['Lead']['folio'];
		
		$nombrePrograma = ucfirst($ArrayConsultarLead['Lead']['nombrePrograma']);
		$nombrePlantel = ucfirst($ArrayConsultarLead['Lead']['nombrePlantel']);
		$nombreNivel = ucfirst($ArrayConsultarLead['Lead']['nombreNivel']);
		$nombreModalidad =ucfirst($ArrayConsultarLead['Lead']['nombreModalidad']);
		
		$promedio = $ArrayConsultarLead['Lead']['promedio'];
		$porcentajeBeca = $ArrayConsultarLead['Lead']['porcentajeBeca'];
		$numeroMensualidades =$ArrayConsultarLead['Lead']['numeroMensualidades'];
		
		
		$inscripcion = number_format($ArrayConsultarLead['Lead']['inscripcion'],2);
		$inscripcionDescuento = number_format($ArrayConsultarLead['Lead']['inscripcionDescuento'],2);
		
		
		
		$descuentoParaFicha = number_format($ArrayConsultarLead['Lead']['descuentoParaFicha'],2);
		$colegiaturaDescuento = number_format($ArrayConsultarLead['Lead']['colegiaturaDescuento'],2);
		$colegiatura = number_format($ArrayConsultarLead['Lead']['colegiatura'],2);
		
	}else{
		
	}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Calculadora 2</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	<style>
		.box-costos{border-bottom:2px solid #C9C9C9}
		.pTitle{color: #ff4801}
		.price{color: #3e3539; margin: 10px 0}
		.folioCalc{color: #ff4801; font-weight:bold; font-size: 16px}
	</style>
</head>
	<div class="container">
		<div class="card my-4">
  			<div class="card-body">
				<h2 class="card-title">Hola <?php echo $nombreCompleto?></h2>
				<?php if($statuslead == "nuevo"){ ?>
					<h3>La solicitud fue enviada a tu correo electronico <?php echo $email?></h3>
				<?php }else{ ?>
					<h3>Gracias por registrarte, un asesor te contactará próximamente.</h3>
					<p>Aquí tienes la última consulta de beca registrada para ti.</p>
				<?php } ?>
				<h2>Tu programa es:</h2>
				<div class="row">
					<div class="col-sm-4">
						<p><strong>Modalidad: </strong><?php echo $nombreModalidad?></p>
						<p><strong>Nivel: </strong><?php echo $nombreNivel?></p>
					</div>
					<div class="col-sm-4">
						<p><strong>Campus: </strong><?php echo $nombrePlantel?></p>
						<p><strong>Programa: </strong><?php echo $nombrePrograma?></p>
					</div>
					<div class="col-sm-4">
						<p><strong>Promedio: </strong><?php echo $promedio?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="card my-4">
  			<div class="card-body">
				<h2>Costo estimado de tu programa:</h2>
				<div class="row">
					<div class="col-sm-8 ">
						<div class="row box-costos my-3">
							<div class="col-sm-6">
								<p class="pTitle">Inscripción:</p>
								<p class="price">$<?php echo $inscripcion?> <small>mxn.</small></p>
							</div>
							<div class="col-sm-6">
								<p class="pTitle">Inscripción con descuento:</p>
								<p class="price">$<?php echo $inscripcionDescuento?> <small>mxn.</small></p>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row box-costos my-3">
							<div class="col-sm-6">
								<p class="pTitle">Colegiatura mensual:</p>
								<p class="price">$<?php echo $colegiatura?> <small>mxn.</small></p>
							</div>
							<div class="col-sm-6">
								<p class="pTitle">Mensualidades:</p>
								<p class="price"><?php echo $numeroMensualidades?></p>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row box-costos my-3">
							<div class="col-sm-6">
								<p class="pTitle">Beca vigente:</p>
								<p class="price"><?php echo $porcentajeBeca?>% Durante toda tu carrera.</p>
							</div>
							<div class="col-sm-6">
								<p class="pTitle">Colegiatura mensual con beca:</p>
								<p class="price">$<?php echo $colegiaturaDescuento?> <small>mxn.</small></p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-4">
						<h5>Menciona a tu asesor IEU este folio <span class="folioCalc"><?php echo $folioCalculadora?></span> para hacerte acreedor a la beca vigente.</h5>
					</div>
				</div>				
			</div>
			<div class="card-footer text-muted">
				<small>*Esta información es de carácter informativo.</small>
				<br>
				<small>Consulta con un asesor IEU los requisitos para hacer acreedor a la beca vigente.</small>
			</div>
		</div>
	</div>
<body>
</body>
</html>
