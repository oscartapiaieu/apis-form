<?php
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://dev03-ieu.cs63.force.com/services/apexrest/generaToken',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "user":"oscar.tapia@ieu.edu.mx",
    "password":"852741"
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: text/plain',
    'Cookie: BrowserId=HWAS4ZiQEeudajncMgaAqg'
  ),
));

$responseToken = curl_exec($curl);
// Comprueba el código de estado HTTP
if (!curl_errno($curl)) {
	switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
		case 200:  # OK
			$items = json_decode($responseToken, true);
			$access_token = $items['access_token'];
		break;
		default:
			$access_token="error";
			echo 'Unexpected HTTP code: ', $http_code, "\n";
	}
}

//GET Modalidad
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/getCatalogo',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
 	"catalogo" : "Modalidad"
}',
   CURLOPT_HTTPHEADER => array(
    'Username: oscar.tapia@ieu.edu.mx',
    'Authorization: Bearer '.$access_token.'',
    'Content-Type: application/json',
    'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
  ),
));

$Modalidad = curl_exec($curl);
// Comprueba el código de estado HTTP
if (!curl_errno($curl)) {
	switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
		case 200:  # OK			
			
		break;
		default:
			$Modalidad='{"Modalidades":[{"Mensaje":"No contamos con modalidad activas."}]}';
			echo 'Unexpected HTTP code: ', $http_code, "\n";
	}
}
// Close handle
curl_close($curl);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">	
<title>Calculadora IEU</title>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<style>
*{font-family: 'Roboto', sans-serif;}
#form-calculadora{position: relative; padding: 15px}
#form-calculadora > div {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between!important;
}
#form-calculadora > div, #form-calculadora > div select[name="plantel"] {
    margin-bottom: 5px;
}
#form-calculadora > div, #form-calculadora > .columns-one > .form-group, #form-calculadora > div input:not([name="avisoPriv"]), #form-calculadora > div select, #form-calculadora > div label {
    width: 100%;
    background-color: transparent;
}
#form-calculadora > div, #form-calculadora > .columns-one > .form-group, #form-calculadora > div input:not([name="avisoPriv"]), #form-calculadora > div select, #form-calculadora > div label {
    width: 100%;
    background-color: transparent;
}
.menu-toggle, button, .ast-button, .ast-custom-button, .button, input#submit, input[type="button"], input[type="submit"], input[type="reset"] {
    border-style: solid;
    border-top-width: 0;
    border-right-width: 0;
    border-left-width: 0;
    border-bottom-width: 0;
    color: #ffffff;
    border-color: rgba(255,255,255,0);
    background-color: rgba(255,255,255,0);
    border-radius: 50px;
    padding-top: 16px;
    padding-right: 40px;
    padding-bottom: 16px;
    padding-left: 40px;
    font-family: inherit;
    font-weight: normal;
    font-size: 14px;
    font-size: 0.875rem;
    text-transform: uppercase;
}
#form-calculadora .form-group {
    margin-right: 0px!important;
}
#form-calculadora > div input:not([name="avisoPriv"]), #form-calculadora > div select {
    border: 1px solid #ff4801;
    /* border-radius: 10px; */
    color: #6e6e6e;
}
input[type=email], input[type=number], input[type=password], input[type=reset], input[type=search], input[type=tel], input[type=text], input[type=url], select, textarea{
    padding: .75em;
    width: -webkit-fill-available !important
}
#form-calculadora > .columns-two > .form-group {
    width: 49%;
}
#form-calculadora .form-group {
    margin-right: 0px!important;
}
#form-calculadora .form-check {
    padding-left: 0px;
    text-align: center;
    text-transform: uppercase;
    font-size: 12px;
}
a, .page-title {
    color: #ff4801;
}
input[type=checkbox], input[type=radio] {
    box-sizing: border-box;
    padding: 0;
}
#form-calculadora #enviar-calc {
    padding: 12px 38px;
    display: block;
    width: auto!important;
    margin: 15px auto 0px;
    background-color: #ff4801;
}

#resp-calc.hidden{
 background-color: #fff;
 border-radius: 10px;
 box-shadow: 0 0 0px 0px rgb(0 0 0 / 0%);
 padding:5px;
 transition: all ease 0.5s;
 display:none;
}

#infoformtitle-calc{margin:0 0 5px; text-align: center; font-weight: bold;}
#infoformtxt-calc{margin:0 0 5px}

#resp-calc.show{
	background-color: #fff;
	display:block;   
    box-shadow: 0 0 13px 0px rgb(0 0 0 / 35%);
	transform: translate(0px, -220px);
	max-width: 200px;
    margin: 0 auto;
    font-size: 11px;
    text-align: center;
	z-index: 9;
	position: relative;
	border-radius: 10px;
	padding: 5px 15px;
}
div#bacblack-calc{
	transition: all ease-in 0.5s;
	width:0px;
	height:0px;
	display:none;
	opacity:0;
	position: absolute;
	top: 0;
}
div#bacblack-calc.show{
	background-color:rgb(0 0 0 / 35%);
	opacity:1;
	height: 300px;
    display: block;
    position: absolute;
    width: 100%;
    top: 0;
	left: 0;
	z-index: 3;
}
button#enviar-calc{cursor: pointer}
button#enviar-calc:disabled{opacity: 0.5; cursor:no-drop}
</style>
</head>

<body>
	<form id="form-calculadora" method="post" onSubmit="return cargarLead_calc(event)">
	<input type="hidden" name="token" value="<?php echo $access_token?>">
    <div class="columns-one">
		<div class="form-group">
			<input class="form-control" id="input-nombre-calc" maxlength="150" name="nombre" placeholder="Nombre completo" required type="text" pattern="[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,254}[ ]{1}[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,30}" title="Por favor escribe tu nombre(s) y apellido y evita números y caracteres especiales."/>
		</div>
	</div>
	<div class="columns-two">
        <div class="form-group">
			<input class="form-control" id="input-telefono-calc" maxlength="15" name="telefono" placeholder="Teléfono*" required type="tel" pattern="[0-9-+]{6,15}" title="Por favor evitar letras o caracteres especiales o espacios en el teléfono."/> 
		</div>
		<div class="form-group">
			<input class="form-control" id="input-mail-calc" maxlength="30" name="mail" placeholder="E-mail*" required type="email" /> 
		</div>		
	</div>
	<div class="columns-two">
        <div class="form-group">
			<select class="form-control" id="input-modalidad-calc" name="modalidad" required onChange="getNiveles_calc()">
				<option value="">Modalidad</option>
			</select>
		</div>

		<div class="form-group">
			<select class="form-control" id="input-nivel-calc" name="nivel" required onChange="getPlantel_calc()"> 
				<option value="" selected>Nivel</option>				
			</select>
		</div>		
	</div>
	<div class="columns-one">
		<div class="form-group">
			<select class="form-control" id="input-plantel-calc" name="plantel" onChange="getPrograma_calc()" required>
				<option value="">Plantel</option>
			</select>
			<script>
								
			</script>
		</div>
		<div class="form-group">
			<select class="form-control" id="input-programa-calc" name="programa" required onChange="loadOfertaEducativa()">
				<option value="">Programa</option>
			</select>
		</div>
		
	</div>
	<div class="columns-one">
		<div class="form-group">
			<input type="number" class="form-control" id="input-promedio-calc" name="promedio" required min="7.0" max="10" step="0.1" onfocusout="beca_calc()" placeholder="Promedio*">
			<script>
				var el = document.getElementById("input-promedio-calc");
				el.value = el.valueAsNumber.toFixed(1);
				el.addEventListener('input', function(event) {
				  event.target.value = event.target.valueAsNumber.toFixed(1);
				});
			</script>
		</div>
	</div>
	<input type="hidden" value="" name="idPeriodo" id="idPeriodo-calc">
	<input type="hidden" value="organico" name="utm_source" id="utm_source-calc">
	<input type="hidden" value="organico" name="utm_medium" id="utm_medium-calc">
	<input type="hidden" value="organico" name="utm_campaign" id="utm_campaign-calc">
	<input type="hidden" value="organico" name="utm_content" id="utm_content-calc">
	<input type="hidden" value="Calcula tu colegiatura" name="subOrigenCandidato" id="subOrigenCandidato">
	<input type="hidden" value="" name="website" id="website-calc">
	<input type="hidden" value="Comercial" name="tipoBeca" id="tipoBeca-calc">
	<input type="hidden" value="" name="letraBeca" id="letraBeca-calc">
	<input type="hidden" value="" name="idBeca" id="idBeca-calc">
	<input type="hidden" value="" name="descuentoInscripcion" id="descuentoInscripcion-calc">
	<input type="hidden" value="" name="idOfertaEducativa" id="idOfertaEducativa-calc">
	<input type="hidden" value="" name="porcentajeDescuento" id="porcentajeDescuento-calc">
	<div class="form-check">
		<label>
			<input id="aceptar-calc" required="true" type="checkbox" name="avisoPriv" /> He le&iacute;do el <a href="#elementor-action%3Aaction%3Dpopup%3Aopen%26settings%3DeyJpZCI6IjMxOTYiLCJ0b2dnbGUiOmZhbHNlfQ%3D%3D">Aviso de privacidad</a> 
		</label>
	</div>
	<button type="submit" class="btn btn-success btn-block" id="enviar-calc" disabled>Enviar</button>
	<div id="bacblack-calc" onClick="cerraraviso_calc()"></div>
	<div id="resp-calc" class="hidden">
		<h3 id="resp_cont_Title-calc"></h3>
		<p id="resp_cont-calc"></p>
	</div>
</form>
<div id="response-calc"></div>
<script>
	//Generales
	const enviar_calc = document.getElementById("enviar-calc");
	const token_calc = "<?php echo $access_token?>";
	const opModalidad_calc = document.getElementById("input-modalidad-calc");
	const opNivel_calc = document.getElementById("input-nivel-calc");
	const opPlantel_calc = document.getElementById("input-plantel-calc");
	const opPrograma_calc = document.getElementById("input-programa-calc");
	const bacblack_calc = document.getElementById("bacblack-calc");
	const resp_calc = document.getElementById("resp-calc");
	const resp_cont_calc = document.getElementById("resp_cont-calc");
	const resp_cont_Title_calc = document.getElementById("resp_cont_Title-calc");
	const formid_calc = document.getElementById("form-calculadora");
	const inp_source_calc = document.getElementById("utm_source-calc");
	const inp_medium_calc = document.getElementById("utm_medium-calc");
	const inp_campaign_calc = document.getElementById("utm_campaign-calc");
	const inp_content_calc = document.getElementById("utm_content-calc");
	const opPromedio_calc = document.getElementById("input-promedio-calc");
	const inp_tipoBeca_calc = document.getElementById("tipoBeca-calc");
	const inp_letraBeca_calc = document.getElementById("letraBeca-calc");
	const inp_idBeca_calc = document.getElementById("idBeca-calc");
	const inp_descuentoInscripcion = document.getElementById("descuentoInscripcion-calc");
	const inp_porcentajeDescuento_calc = document.getElementById("porcentajeDescuento-calc");
	const inp_periodo_calc= document.getElementById("idPeriodo-calc");
	const inp_idOfertaEducativa_calc= document.getElementById("idOfertaEducativa-calc");
	const website_calc= document.getElementById("website-calc");
	const site_calc = window.location.href;
	website_calc.value=site_calc;
	/*ESCUCHAR URL*/
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const utm_source = urlParams.get('utm_source');
	const utm_medium = urlParams.get('utm_medium');
	const utm_campaign = urlParams.get('utm_campaign');
	const utm_content = urlParams.get('utm_content');
	
	//opModalidad in	
	var modalidad_calc = <?php echo $Modalidad?>;
	
	for (var i = 0; i < modalidad_calc.Modalidades.length; i++) {
		option = document.createElement("option");
		option.text = modalidad_calc.Modalidades[i].idValue;
		option.value =modalidad_calc.Modalidades[i].idModalidad;
		option.dataset.periodo = modalidad_calc.Modalidades[i].idPeriodo;
		opModalidad_calc.appendChild(option);
	};
	//opModalidad out	
	//opNivel in	
	var selModalidad_calc;
	function getNiveles_calc(){
		selModalidad_calc = opModalidad_calc.options[opModalidad_calc.selectedIndex].value;
		loadperiodo_calc();
		if(selModalidad_calc != ""){
			cleaninput_calc(opNivel_calc,"Nivel");
			cleaninput_calc(opPlantel_calc,"Plantel");
			cleaninput_calc(opPrograma_calc,"Programa");
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var nivel_calc = JSON.parse(this.responseText);
					for (var i = 0; i < nivel_calc.Niveles.length; i++) {
						optionNivel_calc = document.createElement("option");
						optionNivel_calc.text = nivel_calc.Niveles[i].idValue;
						optionNivel_calc.value =nivel_calc.Niveles[i].idNivel;
						opNivel_calc.appendChild(optionNivel_calc);
					}
				}
			};
			xhttp.open("GET", "niveles.php?token="+token_calc+"&idModalidad="+selModalidad_calc, true);
			xhttp.send();
		}else{
			cleaninput_calc(opNivel_calc,"Nivel");
			cleaninput_calc(opPlantel_calc,"Plantel");
			cleaninput_calc(opPrograma_calc,"Programa");
		}
	};	
	//opNivel out	
	//opPlantel in
	var selNivel_calc;
	function getPlantel_calc(){
		selNivel_calc = opNivel_calc.options[opNivel_calc.selectedIndex].value;
		if(selNivel_calc != ""){
			cleaninput_calc(opPlantel_calc,"Plantel");
			cleaninput_calc(opPrograma_calc,"Programa");
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var plantel_calc = JSON.parse(this.responseText);
					//console.log (plantel.Planteles);
					for (var i = 0; i < plantel_calc.Planteles.length; i++) {
						optionPlantel_calc = document.createElement("option");
						optionPlantel_calc.text = plantel_calc.Planteles[i].idValue;
						optionPlantel_calc.value =plantel_calc.Planteles[i].idPlantel;
						//console.log(plantel.Planteles[i].idValue);
						opPlantel_calc.appendChild(optionPlantel_calc);
					}
					//check input
					var txtModalidad_calc = opModalidad_calc.options[opModalidad_calc.selectedIndex].text;
					if(txtModalidad_calc == "Online" || txtModalidad_calc == "Ejecutiva"){
						opPlantel_calc.selectedIndex = "1";
						opPlantel_calc.style.display = "none";
						getPrograma_calc();
					}else{
						opPlantel_calc.style.display = "block";
					}
				}
			};
			xhttp.open("GET", "plantel.php?token="+token_calc+"&idModalidad="+selModalidad_calc+"&idNivel="+selNivel_calc, true);
			xhttp.send();
		}else{
			cleaninput_calc(opPlantel_calc,"Plantel");
			cleaninput_calc(opPrograma_calc,"Programa");
		};
	};
	//opPlantel out
	//opPrograma in
	var selPlantel_calc;
	function getPrograma_calc(){	
		selPlantel_calc = opPlantel_calc.options[opPlantel_calc.selectedIndex].value;
		if(selPlantel_calc != ""){
			cleaninput_calc(opPrograma_calc,"Programa");
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var programa_calc = JSON.parse(this.responseText);
					//console.log (programa);
					for (var i = 0; i < programa_calc.Programas.length; i++) {
						optionPrograma_calc = document.createElement("option");
						optionPrograma_calc.text = programa_calc.Programas[i].idValue;
						optionPrograma_calc.value =programa_calc.Programas[i].idPrograma;
						//console.log(programa.Programas[i].idValue);
						opPrograma_calc.appendChild(optionPrograma_calc);
					}
				}
			};
			xhttp.open("GET", "programa.php?token="+token_calc+"&idModalidad="+selModalidad_calc+"&idNivel="+selNivel_calc+"&idPlantel="+selPlantel_calc, true);
			xhttp.send();
		}else{
			cleaninput_calc(opPrograma_calc,"Programa");
		}
	};
	//opPrograma out
	//opPromedio in
	var selPromedio_calc;
	var selidOfertaEducativa_calc;
	function beca_calc (){
		selPromedio_calc = opPromedio_calc.value;
		selidOfertaEducativa_calc = inp_idOfertaEducativa_calc.value;
		if(selPromedio_calc != ""){						
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var beca_calc = JSON.parse(this.responseText);
					let longbeca = Object.keys(beca_calc).length;
					if (longbeca != 0){
						var becaId = beca_calc.BecaCalculadora.idBeca;
						var becaCat = beca_calc.BecaCalculadora.categoriaLetra;
						var becaInsc = beca_calc.BecaCalculadora.descuentoInscripcion;
						var becaProm = beca_calc.BecaCalculadora.promedio;
						
						inp_letraBeca_calc.value=becaCat;						
						inp_descuentoInscripcion.value=becaInsc;
						
						if(beca_calc.Beca){
							var becaMensId = beca_calc.Beca.Id;
							var becaMensPorc = beca_calc.Beca[becaCat+"__c"];
							if(becaMensPorc){
								inp_porcentajeDescuento_calc.value=becaMensPorc;
								inp_idBeca_calc.value=becaMensId;
							}else{
								inp_porcentajeDescuento_calc.value="0";
								inp_idBeca_calc.value="";
							}
						}else{
							inp_porcentajeDescuento_calc.value="0";
							inp_idBeca_calc.value="";
						};
						enviar_calc.disabled = false;
					}else{
						inp_letraBeca_calc.value="";
						inp_idBeca_calc.value="";
						inp_descuentoInscripcion.value="0";
						inp_porcentajeDescuento_calc.value="0";
					}
				}
				//console.log(this.responseText);
			};
			xhttp.open("GET", "beca-completo.php?token="+token_calc+"&promedio="+selPromedio_calc+"&idOfertaEducativa="+selidOfertaEducativa_calc, true);
			xhttp.send();
		}else{

		}
	}
	//opPromedio out
	var selopPrograma_calc;
	var selinp_periodo_calc;
	function loadOfertaEducativa(){
		utms();
		selopPrograma_calc = opPrograma_calc.value;
		selinp_periodo_calc = inp_periodo_calc.value;
		var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var OfertaEducativaJson = JSON.parse(this.responseText);
					if (Object.keys(OfertaEducativaJson).length != 0){
						var OfertaEducativaId = OfertaEducativaJson.OfertaEducativa.id;
						var OfertaEducativaName = OfertaEducativaJson.OfertaEducativa.name;
						inp_idOfertaEducativa_calc.value=OfertaEducativaId;
					}else{
						cleaninput_calc(inp_idOfertaEducativa_calc,"");
					}
					
					//console.log(this.responseText);
				}
				opPromedio_calc.value="";
			};
			xhttp.open("GET", "ofertaeducativa.php?token="+token_calc+"&idModalidad="+selModalidad_calc+"&idNivel="+selNivel_calc+"&idPlantel="+selPlantel_calc+"&idPeriodo="+selinp_periodo_calc+"&idPrograma="+selopPrograma_calc, true);
			xhttp.send();
	}
	//add periodo in
	function loadperiodo_calc(){
		selModalidadData_calc = opModalidad_calc.options[opModalidad_calc.selectedIndex].dataset.periodo;
		if(selModalidadData_calc!="" && selModalidadData_calc != undefined){
			//console.log(selModalidadData);
			inp_periodo_calc.value=selModalidadData_calc;
		}
	}
	//add periodo out
	//add idOfertaEducativa in
	
	//add idOfertaEducativa out
	//clean input
	function cleaninput_calc(selectclean_calc,nameinput_calc){			
		for (let i = selectclean_calc.options.length; i >= 0; i--) {
			selectclean_calc.remove(i);
		}
		optionCl_calc = document.createElement("option");
		optionCl_calc.text = nameinput_calc;
		optionCl_calc.value = "";
		selectclean_calc.appendChild(optionCl_calc);
	}
	
	function utms(){
		//INYECTAMOSVALORESDEURL
		if(utm_source != "" && utm_source != null){inp_source_calc.value=utm_source;}
		if(utm_medium != "" && utm_medium != null){inp_medium_calc.value=utm_medium;}
		if(utm_campaign != "" && utm_campaign != null){inp_campaign_calc.value=utm_campaign;}
		if(utm_content != "" && utm_content != null){inp_content_calc.value=utm_content;}
	}
	//
    function cargarLead_calc() {
		event.preventDefault();		
		if(jQuery("#form-calculadora")[0].checkValidity()) {
			var formData = JSON.stringify(jQuery('#form-calculadora').serializeArray());
			var ufrmData = "00-safe-form.php";
			const xhr = new XMLHttpRequest();
			// listen for `load` event
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {			
					// parse JSON
					let mensajelead = JSON.parse(xhr.responseText);
					//console.log(response);
					if(mensajelead.code==400){
						resp_cont_Title_calc.innerHTML = "Lo sentimos.";
						resp_cont_calc.innerHTML = "Tu solicitud no ha sido enviada, prueba nuevamente un unos minutos.";
						console.log(xhr.responseText);
					}
					
					if(mensajelead.idLead){
						var idLead=mensajelead.idLead;
						var tipo= "nuevo";
						console.log("nuevo"+idLead);
					}else if(mensajelead.idLeadExistente){
						var idLead=mensajelead.idLeadExistente;
						var tipo= "registrado";
							console.log("Duplicado"+idLead) 
					}
					
					if(mensajelead.Mensaje=='Guardado' || mensajelead.Mensaje=='Duplicados'){
						resp_cont_Title_calc.innerHTML = "Gracias por contactarnos.";
						resp_cont_calc.innerHTML = "Tu solicitud ha sido enviada y en unos momentos un asesor te contactará.";
					}
					
					window.location.href = "02-calculadora-detalles.php?idLead="+idLead+"&statuslead="+tipo;
					
					   
				}else{
					var formHeight = document.getElementById('form-calculadora').offsetHeight;
					document.getElementById("response-calc").innerHTML = "Cargado...";
					bacblack_calc.style.height = formHeight+"px";
					bacblack_calc.classList.toggle("show");
					resp_calc.classList.remove("hidden");
					resp_calc.classList.add("show");
					resp_cont_Title_calc.innerHTML = "Cargado...";
				}
				document.getElementById("response-calc").innerHTML = xhr.responseText;
			};
			// open request
			xhr.open('POST', ufrmData, true);
			// set `Content-Type` header
			xhr.setRequestHeader('Content-Type', 'application/json');
			// send rquest with x-www payload
			xhr.send(formData);
    	}else{
			console.log("Faltan datos");
		}
	return false;
	};
	function cerraraviso_calc (){
		bacblack_calc.classList.toggle("show");
		resp_calc.classList.remove("show");
		resp_calc.classList.add("hidden");
		resp_cont_Title_calc.innerHTML ="";
		resp_cont_calc.innerHTML = "";
	}
</script>
</body>
</html>