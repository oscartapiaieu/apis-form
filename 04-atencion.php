<?php
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://dev03-ieu.cs63.force.com/services/apexrest/generaToken',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "user":"oscar.tapia@ieu.edu.mx",
    "password":"852741"
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: text/plain',
    'Cookie: BrowserId=HWAS4ZiQEeudajncMgaAqg'
  ),
));

$response = curl_exec($curl);

$items = json_decode($response, true);
$access_token = $items['access_token'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<title>Atención Inmediata</title>
<style>
	html, body{margin: 0; padding: 0}
	body {background-color: #3E3935;}
	div{
		border: 0;
		font-size: 100%;
		font-style: inherit;
		font-weight: inherit;
		margin: 0;
		outline: 0;
		padding: 0;
		vertical-align: baseline;
	}
	form#aten-inmediata {
		display: flex;
		flex-direction: row;
	}
	form#aten-inmediata > div {
		height: auto;
		width: 100%;
	}
	form#aten-inmediata > .fields-wrap > .row {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: space-between;
	}
	form#aten-inmediata > .fields-wrap > .columns-two > div {
		width: 49.3%;
	}
	
	form#aten-inmediata input:not([type="submit"]) {
		padding: 7px 3px;
		font-size: 14px;
		font-style: italic;
		line-height: 16px;
		background-color: transparent;
		border: none;
		border-bottom: 1px solid #FFFFFF;
		border-radius: 0px;
		color: #FFFFFF;
	}
	form#aten-inmediata button#phone-contact-submit {
		padding: 2px 21px;
		font-size: 12px;
		line-height: 15px;
		background-color: #ff4801;
		color: #FFFFFF;
		border-style: solid;
		border-top-width: 0;
		border-right-width: 0;
		border-left-width: 0;
		border-bottom-width: 0;
		border-radius: 50px;
		cursor: pointer;
	}
	form#aten-inmediata button#phone-contact-submit:hover {
		background-color: #db3e01;
	}
	#resp-atn.hidden{
		background-color: #fff;
		border-radius: 10px;
		box-shadow: 0 0 0px 0px rgb(0 0 0 / 0%);
		padding:5px;
		transition: all ease 0.5s;
		display:none;
	}
	#infoformtitle_atn{margin:0 0 5px; text-align: center; font-weight: bold;}
	#infoformtxt_atn{margin:0 0 5px}
	#resp_atn.show{
		background-color: #fff;
		display:block;   
		box-shadow: 0 0 13px 0px rgb(0 0 0 / 35%);
		transform: translate(50%, 3px);
		max-width: 200px;
		margin: 0 auto;
		font-size: 11px;
		text-align: center;
		z-index: 9;
		position: relative;
		border-radius: 10px;
		padding: 10px 15px;
	}
	div#bacblack_atn{
		transition: all ease-in 0.5s;
		width:0px;
		height:0px;
		display:none;
		opacity:0;
		position: absolute;
		top: 0;
	}
	div#bacblack_atn.show{
		background-color:rgb(0 0 0 / 35%);
		opacity:1;
		height: 300px;
		display: block;
		position: absolute;
		width: 100%;
		top: 0;
		left: 0;
		z-index: 3;
	}
	#resp_cont_Title_atn,#resp_cont_atn{margin: 0;}
</style>
</head>

<body>
	<div style="width: 550px">
		<form method="POST" accept-charset="UTF-8" autocomplete="off" id="aten-inmediata" class="phone-contact-form" onSubmit="return cargarLeadInmediata(event)">
			<input name="token" type="hidden" value="<?php echo $access_token?>">
			<input type="hidden" name="retURL" value="https://ieu.edu.mx/?phone_contact=yes">

			<div class="fields-wrap">
				<div class="phone-contact-name-wrapper row columns-two">
					<div>
						<input id="phone_contact_name" tabindex="1" maxlength="90" name="nombre" placeholder="Nombre completo" type="text" required pattern="[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,25}[ ]{1}[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,30}" title="Por favor escribe tu nombre(s) y apellido y evita números y caracteres especiales.">
					</div>
					<div>
						<input id="phone_contact_phone" tabindex="2" maxlength="10" name="telefono" type="tel" placeholder="Teléfono" pattern="[0-9]{10}" title="Ingresa tu número a 10 dígitos. Evita letras y caracteres especiales." required>
					</div>
				</div>
			</div>
			<input type="hidden" value="Atención Inmediata" name="subOrigenCandidato" id="subOrigenCandidato-inmediata">
			<button type="submit" id="phone-contact-submit" class="sprite sprite-phone-contact-submit" data-no-disable="true">ENVIAR</button>
				<div id="bacblack_atn" onClick="cerraraviso_atn()"></div>
			<div id="resp_atn" class="hidden">
				<h3 id="resp_cont_Title_atn"></h3>
				<p id="resp_cont_atn"></p>
			</div>
		</form>
	</div>
<div id="response"></div>
	<script>
		const token = "<?php echo $access_token?>";
		const bacblack_atn = document.getElementById("bacblack_atn");
		const resp_atn = document.getElementById("resp_atn");
		const resp_cont_atn = document.getElementById("resp_cont_atn");
		const formid_atn = document.getElementById("aten-inmediata");
		function cargarLeadInmediata() {
		event.preventDefault();		
		if(jQuery("#aten-inmediata")[0].checkValidity()) {
			var formData = JSON.stringify(jQuery('#aten-inmediata').serializeArray());
			var ufrmData = "04-safe-form-atencion.php";
			const xhr = new XMLHttpRequest();
			// listen for `load` event
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {			
					// parse JSON
					//const response = JSON.parse(xhr.responseText);
					//console.log(response);
					console.log(xhr.responseText)
					document.getElementById("response").innerHTML = xhr.responseText;
					resp_cont_Title_atn.innerHTML = "Información enviada correctamente";
					//formid_atn.reset();
				}else{
					var formHeight = formid_atn.offsetHeight;
					bacblack_atn.style.height = formHeight+"px";
					bacblack_atn.classList.toggle("show");
					resp_atn.classList.remove("hidden");
					resp_atn.classList.add("show");
					resp_cont_Title_atn.innerHTML = "Cargado...";
				}
			};
			// open request
			xhr.open('POST', ufrmData, true);
			// set `Content-Type` header
			xhr.setRequestHeader('Content-Type', 'application/json');
			// send rquest with x-www payload
			xhr.send(formData);
    	}else{
			console.log("Faltan datos");
		}
	return false;
	};
	function cerraraviso_atn (){
		bacblack_atn.classList.toggle("show");
		resp_atn.classList.remove("show");
		resp_atn.classList.add("hidden");
		resp_cont_Title_atn.innerHTML ="";
		resp_cont_atn.innerHTML = "";
	}
	</script>
</body>
</html>