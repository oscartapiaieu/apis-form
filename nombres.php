<?php
if(isset($_GET['nombre'])){
	$full_name =$_GET['nombre'];
}else{
	$full_name = "Oscar de la luz";
}
	
	$nombreCompleto = $full_name;
class NombreSeparados{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
}
function separarNombre($nombreCompleto){
    $partsNombre = new NombreSeparados();    
	/* separar el nombre completo en espacios */
  $tokens = explode(' ', trim($nombreCompleto));
  /* arreglo donde se guardan las "palabras" del nombre */
  $names = array();
  /* palabras de apellidos (y nombres) compuetos */
  $special_tokens = array('da', 'de', 'del', 'la', 'las', 'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa');
  $prev = "";
  foreach($tokens as $token) {
      $_token = strtolower($token);
      if(in_array($_token, $special_tokens)) {
          $prev .= "$token ";
      } else {
          $names[] = $prev. $token;
          $prev = "";
      }
  }
  
  echo $num_nombres = count($names);
  $nombres = $apellidop = $apellidom = "";
	switch ($num_nombres) {
		case 0:
			$nombres = '';
			break;
		case 1: 
			$nombres = $names[0];
			break;
		case 2:
			$nombres    = $names[0];
			$apellidop  = $names[1];
			break;
		case 3:
			$nombres = $names[0];
			$apellidop   = $names[1];
			$apellidom   = $names[2];
			break;
		case 4:
			$nombres = $names[0].' '.$names[1];
			$apellidop   = $names[2];
			$apellidom   = $names[3];
			break;
		default:
			$nombres = $names[0] . ' ' . $names[1];
			unset($names[0]);
			unset($names[1]);
			$apellidop = implode(' ', $names);
			break;
	}
		$partsNombre->nombre = $nombres;
		$partsNombre->apellidoPaterno=$apellidop;
		$partsNombre->apellidoMaterno=$apellidom;
		return $partsNombre;
}

$separarNombre = separarNombre($nombreCompleto);
echo $nombre = $separarNombre->nombre;
echo $apellidoPaterno = $separarNombre->apellidoPaterno;
echo $apellidoMaterno = $separarNombre->apellidoMaterno;
?>
<form method="get" action="nombres.php" accept-charset="UTF-8">
	<input name="nombre" type="text" pattern="[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,25}[ ]{1}[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,30}" title="Por favor escribe tu nombre(s) y apellido y evita números y caracteres especiales." required/>
	<button type="submit">Enviar</button>
</form>