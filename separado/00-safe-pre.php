<?php
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
//header('Content-type:application/json;charset=utf-8');
//print_r($str_json);
//print_r($response[0]);
//print_r($response[0]["value"]);
$longitud = count($response);
$valoresForm=array();
for($i=0;$i<$longitud;$i++){
	$valoresForm[$response[$i]["name"]] = $response[$i]["value"];
}
$Token = $valoresForm["token"];
$Nombre = $valoresForm["nombre"];
$apellidoPaterno = $valoresForm["apellidop"];
$apellidoMaterno = $valoresForm["apellidom"];
$fechanacimiento = $valoresForm["fechanacimiento"];
$sexo = $valoresForm["sexo"];
$edocivil = $valoresForm["edocivil"];
$telefono = $valoresForm["telefono"];
$movil = $valoresForm["cel"];
if (empty($telefono)) {
	$telefono = $movil;
}
$email = $valoresForm["mail"];
$promedio = $valoresForm["promedio"];

$IdPais = $valoresForm["pais"];
$NombrePais = $valoresForm["nombre-pais"];
$IdEstado = $valoresForm["estado"];
$NombreEstado = $valoresForm["nombre-estado"];

$nomexico =false;
if(array_key_exists('municipio', $valoresForm)) {
	$IdMunicipio = $valoresForm["municipio"];
	$NombreMunicipio = $valoresForm["nombre-municipio"];
	$nomexico =true;
}

$idModalidad = $valoresForm["modalidad"];
$idNivel = $valoresForm["nivel"];
$idPlantel = $valoresForm["plantel"];
$idPrograma = $valoresForm["programa"];
$idPeriodo = $valoresForm["idPeriodo"];

$utm_source = $valoresForm["utm_source"];
$utm_medium = $valoresForm["utm_medium"];
$utm_campaign = $valoresForm["utm_campaign"];
$utm_content = $valoresForm["utm_content"];
$subOrigenCandidato = $valoresForm["subOrigenCandidato"];
$medioDifusion = $valoresForm["medioDifusion"];
$website = $valoresForm["website"];
$varURL = $valoresForm["website"];
$avisoPrivacidad = $valoresForm["avisoPriv"];

$tipoBeca = $valoresForm["tipoBeca"];
$letraBeca = $valoresForm["letraBeca"];
$idBeca = $valoresForm["idBeca"];
$porcentajeDescuento = $valoresForm["descuentoInscripcion"];
$porcentajeBeca = $valoresForm["porcentajeDescuento"];



if($avisoPrivacidad=="on"){
	$avisoPrivacidad = "true";
}else{
	$avisoPrivacidad = "false";
}
//SWITCH MODALIDADES
switch ($idModalidad){
	case "a031500000YMu7AAAT"://PRESENCIAL
		$modalidadEjecutiva="false";
		$modalidadOnline="false";
		$modalidadPresencial="true";
	break;
	case "a031500000YMu7CAAT"://ONLINE
		$modalidadEjecutiva="false";
		$modalidadOnline="true";
		$modalidadPresencial="false";
	break;
	case "a031500000YMu7DAAT"://EJECUTIVA
		$modalidadEjecutiva="true";
		$modalidadOnline="false";
		$modalidadPresencial="false";
	break;
	case "a036C000001ty9tQAA"://PRESENCIAL
		$modalidadEjecutiva="false";
		$modalidadOnline="false";
		$modalidadPresencial="true";
	break;
	case "a036C000001ty9vQAA"://ONLINE
		$modalidadEjecutiva="false";
		$modalidadOnline="true";
		$modalidadPresencial="false";
	break;
	case "a036C000001ty9wQAA"://EJECUTIVA
		$modalidadEjecutiva="true";
		$modalidadOnline="false";
		$modalidadPresencial="false";
	break;
	default:
		$modalidadEjecutiva="false";
		$modalidadOnline="false";
		$modalidadPresencial="true";
	break;		
}
//SWITCH MODALIDADES
$vigenciaDescuento=Date('Y-m-d', strtotime('+10 days'));
$sendValues='
 "opcion" : "InsertaLead",  
 "Nombre": "'.$Nombre.'",
 "apellidoPaterno": "'.$apellidoPaterno.'",
 "apellidoMaterno": "'.$apellidoMaterno.'",
 "estadoCivil": "'.$edocivil.'",
 "fechaNacimiento": "'.$fechanacimiento.'",
 "sexo":"'.$sexo.'",
 "status" : "Lead", 
 "telefono" : "'.$telefono.'",
 "telMovil": "'.$movil.'",
 "email": "'.$email.'",
 "idModalidad":"'.$idModalidad.'", 
 "idNivel": "'.$idNivel.'", 
 "idPlantel": "'.$idPlantel.'",
 "idPrograma": "'.$idPrograma.'",
 "idPeriodo": "'.$idPeriodo.'",
 "avisoPrivacidad": '.$avisoPrivacidad.',
 "asignacionAutomatica": true,
 "enviaNotificacion": true,
 "leadSource": "Marketing",
 "website": "'.$website.'",
 "varURL": "'.$website.'",
 "subOrigenCandidato": "'.$subOrigenCandidato.'",
 "modalidadEjecutiva": '.$modalidadEjecutiva.',
 "modalidadOnline": '.$modalidadOnline.',
 "modalidadPresencial": '.$modalidadPresencial.',
 "utmSource": "'.$utm_source.'",
 "utmMedium": "'.$utm_medium.'",
 "utmCampaign": "'.$utm_campaign.'",
 "utmContent": "'.$utm_content.'",
 "porcentajeDescuento": '.$porcentajeDescuento.',
 "porcentajeBeca": "'.$porcentajeBeca.'",
 "tipoBeca": "'.$tipoBeca.'",
 "promedio": "'.$promedio.'",
 "letraBeca": "'.$letraBeca.'",
 "idBeca": "'.$idBeca.'",
 "vigenciaDescuento": "'.$vigenciaDescuento.'",
 "idPais": "'.$IdPais.'",
 "pais": "'.$NombrePais.'",
 "idEstado": "'.$IdEstado.'",
 "estado": "'.$NombreEstado.'",
 "medioDifusion":"'.$medioDifusion.'"
';
if(array_key_exists('municipio', $valoresForm)) {
	
	$sendValues.=',
		"idMunicipio": "'.$IdMunicipio.'",
		"municipio": "'.$NombreMunicipio.'"
	';
}
//print_r($sendValues);
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/manageLead',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{'.$sendValues.'}',
  CURLOPT_HTTPHEADER => array(
    'Username: oscar.tapia@ieu.edu.mx',
    'Authorization: Bearer '.$Token.'',
    'Content-Type: application/json',
    'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
  ),
));


$response = curl_exec($curl);
curl_close($curl);
echo $response;

//nuevo
//echo '{"idLead": "00Q6C00000OFxZqUAL","Mensaje": "Guardado"}';
//Duplicados
//echo '{"idLead": null,"Mensaje": "Duplicados","idLeadExistente": "00Q6C00000OFxN2UAL"}';

?>
