<?php
	require('token.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Flexible Formulario</title>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>
<style>
*{font-family: 'Roboto', sans-serif;}
.form{position: relative; padding: 15px}
.form > div {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between!important;
}
.form > div, .form > div select[name="plantel"] {
    margin-bottom: 5px;
}
.form-flexible > div, .form > .columns-one > .form-group, .form > div input:not([name="avisoPriv"]), .form > div select, .form > div label {
    width: 100%;
    background-color: transparent;
}
.form > div, .form > .columns-one > .form-group, .form > div input:not([name="avisoPriv"]), .form > div select, .form > div label {
    width: 100%;
    background-color: transparent;
}
.menu-toggle, button, .ast-button, .ast-custom-button, .button, input#submit, input[type="button"], input[type="submit"], input[type="reset"] {
    border-style: solid;
    border-top-width: 0;
    border-right-width: 0;
    border-left-width: 0;
    border-bottom-width: 0;
    color: #ffffff;
    border-color: rgba(255,255,255,0);
    background-color: rgba(255,255,255,0);
    border-radius: 50px;
    padding-top: 16px;
    padding-right: 40px;
    padding-bottom: 16px;
    padding-left: 40px;
    font-family: inherit;
    font-weight: normal;
    font-size: 14px;
    font-size: 0.875rem;
    text-transform: uppercase;
}
.form .form-group {
    margin-right: 0px!important;
}
.form > div input:not([name="avisoPriv"]), .form > div select {
    border: 1px solid #ff4801;
    /* border-radius: 10px; */
    color: #6e6e6e;
}
input[type=email], input[type=number], input[type=password], input[type=reset], input[type=search], input[type=tel], input[type=text], input[type=url], select, textarea{
    padding: .75em;
    width: -webkit-fill-available !important
}
.form > .columns-two > .form-group {
    width: 49%;
}
.form .form-group {
    margin-right: 0px!important;
}
.form .form-check {
    padding-left: 0px;
    text-align: center;
    text-transform: uppercase;
    font-size: 12px;
}
a, .page-title {
    color: #ff4801;
}
input[type=checkbox], input[type=radio] {
    box-sizing: border-box;
    padding: 0;
}
.form .enviar {
    padding: 12px 38px;
    display: block;
    width: auto!important;
    margin: 15px auto 0px;
    background-color: #ff4801;
}

.resp.hidden{
 background-color: #fff;
 border-radius: 10px;
 box-shadow: 0 0 0px 0px rgb(0 0 0 / 0%);
 padding:5px;
 transition: all ease 0.5s;
 display:none;
}

.infoformtitle{margin:0 0 5px; text-align: center; font-weight: bold;}
.infoformtxt{margin:0 0 5px}

.resp.show{
	background-color: #fff;
	display:block;   
    box-shadow: 0 0 13px 0px rgb(0 0 0 / 35%);
	transform: translate(0px, -220px);
	max-width: 200px;
    margin: 0 auto;
    font-size: 11px;
    text-align: center;
	z-index: 9;
	position: relative;
	border-radius: 10px;
	padding: 5px 15px;
}
div.bacblack{
	transition: all ease-in 0.5s;
	width:0px;
	height:0px;
	display:none;
	opacity:0;
	position: absolute;
	top: 0;
}
div.bacblack.show{
	background-color:rgb(0 0 0 / 35%);
	opacity:1;
	height: 300px;
    display: block;
    position: absolute;
    width: 100%;
    top: 0;
	left: 0;
	z-index: 3;
}
button.enviar{cursor: pointer}
button.enviar:disabled{opacity: 0.5; cursor:no-drop}
</style>
<body>
<form id="form-flexible" class="form" method="post" onSubmit="return cargarLead(event)">
	<input type="hidden" name="token" id="token" value="">
    <div class="columns-one">
		<div class="form-group">
			<input class="form-control" id="input-nombre" maxlength="150" name="nombre" placeholder="Nombre(s) Apellidos*" required type="text" pattern="[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,254}" title="Por favor evitar números o caracteres especiales en el nombre."/>
		</div>
	</div>
	<div class="columns-two">
        <div class="form-group">
			<input class="form-control" id="input-telefono" maxlength="10" name="telefono" placeholder="Teléfono*" required type="tel" pattern="[0-9-+]{6,10}" title=" Ingresa tu teléfono a 10 digitos. Por favor evitar letras o caracteres especiales o espacios en el teléfono." onkeydown="limit_lp(this);" onkeyup="limit_lp(this);"/> 
		</div>
		<div class="form-group">
			<input class="form-control" id="input-mail" maxlength="30" name="mail" placeholder="E-mail*" required type="email" /> 
		</div>		
	</div>
	<div class="columns-two">
        <div class="form-group">
			<select class="form-control" id="input-modalidad" name="modalidad" required onChange="getNiveles()">
				<option value="">Modalidad</option>
			</select>
		</div>

		<div class="form-group">
			<select class="form-control" id="input-nivel" name="nivel" required onChange="getPlantel()"> 
				<option value="" selected>Nivel</option>				
			</select>
		</div>		
	</div>
	<div class="columns-one">
		<div class="form-group">
			<select class="form-control" id="input-plantel" name="plantel" onChange="getPrograma()" required>
				<option value="">Plantel</option>
			</select>
			<script>
								
			</script>
		</div>
		<div class="form-group">
			<select class="form-control" id="input-programa" name="programa" required onChange="cargaPrograma()">
				<option value="" >Programa</option>
			</select>
		</div>
	</div>
	<input type="hidden" value="" name="idPeriodo" id="idPeriodo">
	<input type="hidden" value="organico" name="utm_source" id="utm_source">
	<input type="hidden" value="organico" name="utm_medium" id="utm_medium">
	<input type="hidden" value="organico" name="utm_campaign" id="utm_campaign">
	<input type="hidden" value="organico" name="utm_content" id="utm_content">
	<input type="hidden" value="Flexible-LP" name="subOrigenCandidato" id="subOrigenCandidato">
	<input type="hidden" value="" name="website" id="website">
	<div class="form-check">
		<label>
			<input id="aceptar" required="true" type="checkbox" name="avisoPriv" /> He le&iacute;do el <a href="#elementor-action%3Aaction%3Dpopup%3Aopen%26settings%3DeyJpZCI6IjMxOTYiLCJ0b2dnbGUiOmZhbHNlfQ%3D%3D">Aviso de privacidad</a> 
		</label>
	</div>
	<button type="submit" class="btn btn-success btn-block enviar" id="enviar" disabled>Enviar</button>
	<div id="bacblack" class="bacblack" onClick="cerraraviso()"></div>
	<div id="resp" class="resp hidden">
		<h3 id="resp_cont_Title" class="resp_cont_Title"></h3>
		<p id="resp_cont" class="resp_cont"></p>
	</div>
</form>
<div id="response"></div>


<script>
	function limit_lp(element){
    	var max_chars = 10;
    	if(element.value.length > max_chars) {
        	element.value = element.value.substr(0, max_chars);
    	}
	}
	//Generales
	const inptoken = document.getElementById("token");
	const enviar = document.getElementById("enviar");
	const opModalidad = document.getElementById("input-modalidad");
	const opNivel = document.getElementById("input-nivel");
	const opPlantel = document.getElementById("input-plantel");
	const opPrograma = document.getElementById("input-programa");
	const bacblack = document.getElementById("bacblack");
	const resp = document.getElementById("resp");
	const resp_cont = document.getElementById("resp_cont");
	const resp_cont_Title = document.getElementById("resp_cont_Title");
	const formid = document.getElementById("form-flexible");
	const inp_source= document.getElementById("utm_source");
	const inp_medium= document.getElementById("utm_medium");
	const inp_campaign= document.getElementById("utm_campaign");
	const inp_content= document.getElementById("utm_content");
	const inp_periodo= document.getElementById("idPeriodo");
	const website= document.getElementById("website");
	const site = window.location.href;
	/*ESCUCHAR URL*/
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	//INYECTAMOSVALORESDEURL
	const utm_source = urlParams.get('utm_source');
	const utm_medium = urlParams.get('utm_medium');
	const utm_campaign = urlParams.get('utm_campaign');
	const utm_content = urlParams.get('utm_content');
	//opModalidad in
	function getModalidad() {
		website.value=site;
		inptoken.value = token;
		var xhttp = new XMLHttpRequest();						
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var modalidad = JSON.parse(this.responseText);
				//console.log (programa);
				for (var i = 0; i < modalidad.Modalidades.length; i++) {
					option = document.createElement("option");
					option.text = modalidad.Modalidades[i].idValue;
					option.value = modalidad.Modalidades[i].idModalidad;
					option.dataset.periodo = modalidad.Modalidades[i].idPeriodo;
					opModalidad.appendChild(option);
				};
			}
		};
		xhttp.open("GET", "modalidad.php?token="+token, true);
		xhttp.send();
		utms();
	}
	window.onload = getModalidad;
	//opModalidad out	
	
	//opNivel in	
	var selModalidad;
	function getNiveles(){
		
		selModalidad = opModalidad.options[opModalidad.selectedIndex].value;
		loadperiodo();
		if(selModalidad != ""){
			cleaninput(opNivel,"Nivel");
			cleaninput(opPlantel,"Plantel");
			cleaninput(opPrograma,"Programa");
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var nivel = JSON.parse(this.responseText);
					for (var i = 0; i < nivel.Niveles.length; i++) {
						optionNivel = document.createElement("option");
						optionNivel.text = nivel.Niveles[i].idValue;
						optionNivel.value =nivel.Niveles[i].idNivel;
						opNivel.appendChild(optionNivel);
					}
				}
			};
			xhttp.open("GET", "niveles.php?token="+token+"&idModalidad="+selModalidad, true);
			xhttp.send();
		}else{
			cleaninput(opNivel,"Nivel");
			cleaninput(opPlantel,"Plantel");
			cleaninput(opPrograma,"Programa");
		}
	};	
	//opNivel out	
	//opPlantel in
	var selNivel;
	function getPlantel(){
		selNivel = opNivel.options[opNivel.selectedIndex].value;
		if(selNivel != ""){
			cleaninput(opPlantel,"Plantel");
			cleaninput(opPrograma,"Programa");
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var plantel = JSON.parse(this.responseText);
					//console.log (plantel.Planteles);
					for (var i = 0; i < plantel.Planteles.length; i++) {
						optionPlantel = document.createElement("option");
						optionPlantel.text = plantel.Planteles[i].idValue;
						optionPlantel.value =plantel.Planteles[i].idPlantel;
						//console.log(plantel.Planteles[i].idValue);
						opPlantel.appendChild(optionPlantel);
					}
					//check input
					var txtModalidad = opModalidad.options[opModalidad.selectedIndex].text;
					if(txtModalidad == "Online" || txtModalidad == "Ejecutiva"){
						opPlantel.selectedIndex = "1";
						opPlantel.style.display = "none";
						getPrograma();
					}else{
						opPlantel.style.display = "block";
					}
				}
			};
			xhttp.open("GET", "plantel.php?token="+token+"&idModalidad="+selModalidad+"&idNivel="+selNivel, true);
			xhttp.send();
		}else{
			cleaninput(opPlantel,"Plantel");
			cleaninput(opPrograma,"Programa");
		};
	};
	//opPlantel out
	//opPrograma in
	var selPlantel;
	function getPrograma(){	
		selPlantel = opPlantel.options[opPlantel.selectedIndex].value;
		if(selPlantel != ""){
			cleaninput(opPrograma,"Programa");
			var xhttp = new XMLHttpRequest();						
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var programa = JSON.parse(this.responseText);
					//console.log (programa);
					for (var i = 0; i < programa.Programas.length; i++) {
						optionPrograma = document.createElement("option");
						optionPrograma.text = programa.Programas[i].idValue;
						optionPrograma.value =programa.Programas[i].idPrograma;
						//console.log(programa.Programas[i].idValue);
						opPrograma.appendChild(optionPrograma);
					}
				}
			};
			xhttp.open("GET", "programa.php?token="+token+"&idModalidad="+selModalidad+"&idNivel="+selNivel+"&idPlantel="+selPlantel, true);
			xhttp.send();
		}else{
			cleaninput(opPrograma,"Programa");
		}
	};
	//opPrograma out
	//add periodo in
	function loadperiodo(){
		selModalidadData = opModalidad.options[opModalidad.selectedIndex].dataset.periodo;
		if(selModalidadData!="" && selModalidadData != undefined){
			//console.log(selModalidadData);
			inp_periodo.value=selModalidadData;
		}
		
	}
	//add periodo out
	function cargaPrograma(){
		enviar.disabled = false;
	}
	//clean input
		function cleaninput(selectclean,nameinput){			
			for (let i = selectclean.options.length; i >= 0; i--) {
				selectclean.remove(i);
			}
			optionCl = document.createElement("option");
			optionCl.text = nameinput;
			optionCl.value = "";
			selectclean.appendChild(optionCl);
		}
	//
    function cargarLead() {
		event.preventDefault();
		enviar.disabled = true;
		if(jQuery("#form-flexible")[0].checkValidity()) {
			var formData = JSON.stringify(jQuery('#form-flexible').serializeArray());
			var ufrmData = "00-safe-form.php";
			const xhr = new XMLHttpRequest();
			// listen for `load` event
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {			
					// parse JSON
					//console.log(xhr.responseText);
					let respuesta = JSON.parse(xhr.responseText);
					if(respuesta.code==400){
						resp_cont_Title.innerHTML = "Lo sentimos.";
						resp_cont.innerHTML = "Tu solicitud no ha sido enviada, prueba nuevamente un unos minutos.";
					}
					if(respuesta.Mensaje=='Guardado'){
						document.getElementById("response").innerHTML = xhr.responseText;
						resp_cont_Title.innerHTML = "Gracias por contactarnos.";
						resp_cont.innerHTML = "Tu solicitud ha sido enviada y en unos momentos un asesor te contactará.";
						formid.reset();
						opPlantel.style.display = "block";
					}
					//console.log(xhr.responseText);
				}else{
					if(this.status == 400){
						resp_cont_Title.innerHTML = "Lo sentimos.";
						resp_cont.innerHTML = "Tu solicitud no ha sido enviada, prueba nuevamente un unos minutos.";
					}
					var formHeight = document.getElementById('form-flexible').offsetHeight;
					document.getElementById("response").innerHTML = "Cargado...";
					bacblack.style.height = formHeight+"px";
					bacblack.classList.toggle("show");
					resp.classList.remove("hidden");
					resp.classList.add("show");
					resp_cont_Title.innerHTML = "Cargado...";
					enviar.disabled = true;
				}
			};
			// open request
			xhr.open('POST', ufrmData, true);
			// set `Content-Type` header
			xhr.setRequestHeader('Content-Type', 'application/json');
			// send rquest with x-www payload
			xhr.send(formData);
    	}else{
			var formHeight = document.getElementById('form-flexible').offsetHeight;
			bacblack.style.height = formHeight+"px";
			bacblack.classList.toggle("show");
			resp.classList.remove("hidden");
			resp.classList.add("show");
			resp_cont_Title.innerHTML = "Por favor completa todos los datos requeridos.";
			//console.log("Por favor completa todos los datos requeridos.");
			enviar.disabled = false;
		}
	return false;
	};
	function utms(){
		//INYECTAMOSVALORESDEURL
		if(utm_source != "" && utm_source != null){inp_source_calc.value=utm_source;}
		if(utm_medium != "" && utm_medium != null){inp_medium_calc.value=utm_medium;}
		if(utm_campaign != "" && utm_campaign != null){inp_campaign_calc.value=utm_campaign;}
		if(utm_content != "" && utm_content != null){inp_content_calc.value=utm_content;}
	}
	//
	function cerraraviso (){
		bacblack.classList.toggle("show");
		resp.classList.remove("show");
		resp.classList.add("hidden");
		resp_cont_Title.innerHTML ="";
		resp_cont.innerHTML = "";
	}
</script>


</body>
</html>