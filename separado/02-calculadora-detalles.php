<?php
	if(isset($_GET['idLead']) AND !empty($_GET['idLead'])){
		$idLead=$_GET['idLead'];
		if(isset($_GET['statuslead'])){
			$statuslead = $_GET['statuslead'];
		}else{
			$statuslead="nuevo";
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://dev03-ieu.cs63.force.com/services/apexrest/generaToken',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{
			"user" : "oscar.tapia@ieu.edu.mx",
			"password" : "852741"
		}',
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Cookie: BrowserId=HWAS4ZiQEeudajncMgaAqg'
		  ),
		));

		$responseToken = curl_exec($curl);
		// Comprueba el código de estado HTTP
		if (!curl_errno($curl)) {
			switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
				case 200:  # OK
					$items = json_decode($responseToken, true);
					$access_token = $items['access_token'];
				break;
				default:
					header('Location: 02-calculadora.php');
					echo 'Unexpected HTTP code: ', $http_code, "\n";
			}
		}
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/manageLead',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{
			"opcion" : "consultarLead",
			"idLead" : "'.$idLead.'"
		}',
		  CURLOPT_HTTPHEADER => array(
			'Username: oscar.tapia@ieu.edu.mx',
			'Authorization: Bearer '.$access_token.'',
			'Content-Type: application/json',
			'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
		  ),
		));

		$ConsultarLead = curl_exec($curl);
		if (!curl_errno($curl)) {
			switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
				case 200:  # OK
					$ArrayConsultarLead = json_decode($ConsultarLead, true);
					$leadEncontrado=true;
				break;
				default:
					$leadEncontrado=false;
					echo 'Unexpected HTTP code: ', $http_code, "\n";
			}
		}
		curl_close($curl);
	}else{
		header('Location: 02-calculadora.php');
	}
//print_r($ArrayConsultarLead);
	if($leadEncontrado==true){
		$idLead = $ArrayConsultarLead['Lead']['idLead'];
		$nombre = $ArrayConsultarLead['Lead']['nombre'];
		$apellidoPaterno = $ArrayConsultarLead['Lead']['apellidoPaterno'];
		$nombreCompleto = $nombre.' '.$apellidoPaterno;
		if (array_key_exists('apellidoMaterno', $ArrayConsultarLead['Lead'])) {
			$apellidoMaterno = $ArrayConsultarLead['Lead']['apellidoMaterno'];
			$nombreCompleto .= ' '.$apellidoMaterno;
		}
		$telefono = $ArrayConsultarLead['Lead']['telefono'];
		$email = $ArrayConsultarLead['Lead']['email'];	
		$folio = $ArrayConsultarLead['Lead']['folio'];
		if (array_key_exists('folioCalculadora', $ArrayConsultarLead['Lead'])) {
			$folioCalculadora = $ArrayConsultarLead['Lead']['folioCalculadora'];
		}else{
			$folioCalculadora=$folio;
		}				
		$nombrePrograma = ucfirst($ArrayConsultarLead['Lead']['nombrePrograma']);
		$nombrePlantel = ucfirst($ArrayConsultarLead['Lead']['nombrePlantel']);
		$nombreNivel = ucfirst($ArrayConsultarLead['Lead']['nombreNivel']);
		$nombreModalidad =ucfirst($ArrayConsultarLead['Lead']['nombreModalidad']);
		
		if (array_key_exists('promedio', $ArrayConsultarLead['Lead'])) {
			$promedio = $ArrayConsultarLead['Lead']['promedio'];
		}else{
			$promedio="-";
		}
		if (array_key_exists('porcentajeBeca', $ArrayConsultarLead['Lead'])) {
			$porcentajeBeca = $ArrayConsultarLead['Lead']['porcentajeBeca'];
		}else{
			$porcentajeBeca="0";
		}		
		$numeroMensualidades =$ArrayConsultarLead['Lead']['numeroMensualidades'];
		$inscripcion = number_format($ArrayConsultarLead['Lead']['inscripcion'],2);
		if (array_key_exists('inscripcionDescuento', $ArrayConsultarLead['Lead']) OR ($ArrayConsultarLead['Lead']['inscripcionDescuento']=="") ) {
			$inscripcionDescuento = number_format($ArrayConsultarLead['Lead']['inscripcionDescuento'],2);
		}else{
			$inscripcionDescuento = $inscripcion;
		}
		$descuentoParaFicha = number_format($ArrayConsultarLead['Lead']['descuentoParaFicha'],2);
		$colegiaturaDescuento = number_format($ArrayConsultarLead['Lead']['colegiaturaDescuento'],2);
		$colegiatura = number_format($ArrayConsultarLead['Lead']['colegiatura'],2);
		
		if($statuslead == "nuevo"){
			$saludo = "La solicitud fue enviada a tu correo electronico $email";
			$saludosub="";
		}else{
			$saludo= "Gracias por registrarte, un asesor te contactará próximamente.";
			$saludosub="Aquí tienes la última consulta de beca registrada para ti.";
		}
		
		echo '<script>
			var idLead = "'.$idLead.'";
			var nombreCompleto = "'.$nombreCompleto.'";
			var telefono = "'.$telefono.'";
			var email = "'.$email.'";
			var folioCalculadora = "'.$folioCalculadora.'";
			var folio = "'.$folio.'";
			var nombrePrograma = "'.$nombrePrograma.'";
			var nombrePlantel = "'.$nombrePlantel.'";
			var nombreNivel = "'.$nombreNivel.'";
			var nombreModalidad = "'.$nombreModalidad.'";
			var promedio = "'.$promedio.'";
			var porcentajeBeca = "'.$porcentajeBeca.'";
			var numeroMensualidades = "'.$numeroMensualidades.'";
			var inscripcion = "'.$inscripcion.'";
			var inscripcionDescuento = "'.$inscripcionDescuento.'";
			var descuentoParaFicha = "'.$descuentoParaFicha.'";
			var colegiaturaDescuento = "'.$colegiaturaDescuento.'";
			var colegiatura = "'.$colegiatura.'";
			var saludo = "'.$saludo.'";
			var saludosub = "'.$saludosub.'";
		</script>';
	}else{
		
	}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Calculadora 2</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	<style>
		.box-costos{border-bottom:2px solid #C9C9C9}
		.pTitle{color: #ff4801}
		.price{color: #3e3539; margin: 10px 0}
		.folioCalc{color: #ff4801; font-weight:bold; font-size: 16px}
	</style>
</head>
<body>
	<div class="container">
		<div class="card my-4">
  			<div class="card-body">
				<h2 class="card-title">Hola <span id="nombreCompleto-calc"></span></h2>
				<h3 id="saludo-calc"></h3>
				<p id="saludosub-calc"></p>
				<h2>Tu programa es:</h2>
				<div class="row">
					<div class="col-sm-4">
						<p><strong>Modalidad: </strong> <span id="nombreModalidad-calc"></span></p>
						<p><strong>Nivel: </strong> <span id="nombreNivel-calc"></span></p>
					</div>
					<div class="col-sm-4">
						<p><strong>Campus: </strong> <span id="nombrePlantel-calc"></span></p>
						<p><strong>Programa: </strong> <span id="nombrePrograma-calc"></span></p>
					</div>
					<div class="col-sm-4">
						<p><strong>Promedio: </strong> <span id="promedio-calc"></span></p>
					</div>
				</div>
			</div>
		</div>
		<div class="card my-4">
  			<div class="card-body">
				<h2>Costo estimado de tu programa:</h2>
				<div class="row">
					<div class="col-sm-8 ">
						<div class="row box-costos my-3">
							<div class="col-sm-6">
								<p class="pTitle">Inscripción:</p>
								<p class="price">$<span id="inscripcion-calc"></span> <small>mxn.</small></p>
							</div>
							<div class="col-sm-6">
								<p class="pTitle">Inscripción con descuento:</p>
								<p class="price">$<span id="inscripcionDescuento-calc"></span> <small>mxn.</small></p>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row box-costos my-3">
							<div class="col-sm-6">
								<p class="pTitle">Colegiatura mensual:</p>
								<p class="price">$<span id="colegiatura-calc"></span> <small>mxn.</small></p>
							</div>
							<div class="col-sm-6">
								<p class="pTitle">Mensualidades:</p>
								<p class="price"><span id="numeroMensualidades-calc"></span></p>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row box-costos my-3">
							<div class="col-sm-6">
								<p class="pTitle">Beca vigente:</p>
								<p class="price"><span id="porcentajeBeca-calc"></span>% Durante toda tu carrera.</p>
							</div>
							<div class="col-sm-6">
								<p class="pTitle">Colegiatura mensual con beca:</p>
								<p class="price">$<span id="colegiaturaDescuento-calc"></span> <small>mxn.</small></p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-4">
						<h5>Menciona a tu asesor IEU este folio <span class="folioCalc" id="folioCalculadora-calc"></span> para hacerte acreedor a la beca vigente.</h5>
					</div>
				</div>				
			</div>
			<div class="card-footer text-muted">
				<small>*Esta información es de carácter informativo.</small>
				<br>
				<small>Consulta con un asesor IEU los requisitos para hacer acreedor a la beca vigente.</small>
			</div>
		</div>
	</div>
<script>
	window.onload = function() {
		const nombreCompleto_calc = document.getElementById("nombreCompleto-calc");
		const saludo_calc = document.getElementById("saludo-calc");
		const saludosub_calc = document.getElementById("saludosub-calc");
		const nombreModalidad_calc = document.getElementById("nombreModalidad-calc");
		const nombreNivel_calc = document.getElementById("nombreNivel-calc");
		const nombrePlantel_calc = document.getElementById("nombrePlantel-calc");		
		const nombrePrograma_calc = document.getElementById("nombrePrograma-calc");
		const promedio_calc = document.getElementById("promedio-calc");
		const inscripcion_calc = document.getElementById("inscripcion-calc");
		const inscripcionDescuento_calc = document.getElementById("inscripcionDescuento-calc");		
		const colegiatura_calc = document.getElementById("colegiatura-calc");
		const numeroMensualidades_calc = document.getElementById("numeroMensualidades-calc");
		const porcentajeBeca_calc = document.getElementById("porcentajeBeca-calc");		
		const colegiaturaDescuento_calc = document.getElementById("colegiaturaDescuento-calc");
		const folioCalculadora_calc = document.getElementById("folioCalculadora-calc");
		
		nombreCompleto_calc.innerHTML =nombreCompleto;		
		saludo_calc.innerHTML =saludo;
		saludosub_calc.innerHTML =saludosub;
		nombreModalidad_calc.innerHTML =nombreModalidad;
		nombreNivel_calc.innerHTML =nombreNivel;
		nombrePlantel_calc.innerHTML =nombrePlantel;
		nombrePrograma_calc.innerHTML =nombrePrograma;
		promedio_calc.innerHTML =promedio;
		inscripcion_calc.innerHTML =inscripcion;
		inscripcionDescuento_calc.innerHTML =inscripcionDescuento;
		colegiatura_calc.innerHTML =colegiatura;
		numeroMensualidades_calc.innerHTML =numeroMensualidades;
		porcentajeBeca_calc.innerHTML =porcentajeBeca;
		colegiaturaDescuento_calc.innerHTML =colegiaturaDescuento;
		folioCalculadora_calc.innerHTML =folioCalculadora;
		console.log(nombreCompleto);
	}
</script>
</body>
</html>
