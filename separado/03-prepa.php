<?php
	require('token.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Formulario Prepa</title>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<style>
*{font-family: 'Roboto', sans-serif;}
.form{position: relative; padding: 15px}
.form > div {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between!important;
}
.form > div, .form > div select[name="plantel"] {
    margin-bottom: 5px;
}
.form > div, .form > .columns-one > .form-group, .form > div input:not([name="avisoPriv"]), .form > div select, .form > div label {
    width: 100%;
    background-color: transparent;
}
.form > div, .form > .columns-one > .form-group, .form > div input:not([name="avisoPriv"]), .form > div select, .form > div label {
    width: 100%;
    background-color: transparent;
}
.menu-toggle, button, .ast-button, .ast-custom-button, .button, input#submit-prepa, input[type="button"], input[type="submit"], input[type="reset"] {
    border-style: solid;
    border-top-width: 0;
    border-right-width: 0;
    border-left-width: 0;
    border-bottom-width: 0;
    color: #ffffff;
    border-color: rgba(255,255,255,0);
    background-color: rgba(255,255,255,0);
    border-radius: 50px;
    padding-top: 16px;
    padding-right: 40px;
    padding-bottom: 16px;
    padding-left: 40px;
    font-family: inherit;
    font-weight: normal;
    font-size: 14px;
    font-size: 0.875rem;
    text-transform: uppercase;
}
.form .form-group {
    margin-right: 0px!important;
}
.form > div input:not([name="avisoPriv"]), .form > div select {
    border: 1px solid #ff4801;
    /* border-radius: 10px; */
    color: #6e6e6e;
}
input[type=email], input[type=number], input[type=password], input[type=reset], input[type=search], input[type=tel], input[type=text], input[type=url], select, textarea{
    padding: .75em;
    width: -webkit-fill-available !important
}
.form > .columns-two > .form-group {
    width: 49%;
}
.form .form-group {
    margin-right: 0px!important;
}
.form .form-check {
    padding-left: 0px;
    text-align: center;
    text-transform: uppercase;
    font-size: 12px;
}
a, .page-title {
    color: #ff4801;
}
input[type=checkbox], input[type=radio] {
    box-sizing: border-box;
    padding: 0;
}
.form .enviar {
    padding: 12px 38px;
    display: block;
    width: auto!important;
    margin: 15px auto 0px;
    background-color: #ff4801;
}

.resp.hidden{
 background-color: #fff;
 border-radius: 10px;
 box-shadow: 0 0 0px 0px rgb(0 0 0 / 0%);
 padding:5px;
 transition: all ease 0.5s;
 display:none;
}

.infoformtitle{margin:0 0 5px; text-align: center; font-weight: bold;}
.infoformtxt{margin:0 0 5px}

.resp.show{
	background-color: #fff;
	display:block;   
    box-shadow: 0 0 13px 0px rgb(0 0 0 / 35%);
	transform: translate(0px, -220px);
	max-width: 200px;
    margin: 0 auto;
    font-size: 11px;
    text-align: center;
	z-index: 9;
	position: relative;
	border-radius: 10px;
	padding: 5px 15px;
}
div.bacblack{
	transition: all ease-in 0.5s;
	width:0px;
	height:0px;
	display:none;
	opacity:0;
	position: absolute;
	top: 0;
}
div.bacblack.show{
	background-color:rgb(0 0 0 / 35%);
	opacity:1;
	height: 300px;
    display: block;
    position: absolute;
    width: 100%;
    top: 0;
	left: 0;
	z-index: 3;
}
button.enviar{cursor: pointer}
</style>
</head>

<body>
	<form id="form-prepa" method="post" class="form" onSubmit="return cargarPrepa(event)">
	<input type="hidden" name="token" id="token-prepa" value="">
    <div class="columns-one">
		<div class="form-group">
			<input class="form-control" id="input-nombre-prepa" maxlength="150" name="nombre" placeholder="Nombre(s) Apellidos*" required type="text" pattern="[a-zA-ZáéíúóÁÉÍÓÚñÑ ]{2,254}" title="Por favor evitar números o caracteres especiales en el nombre."/>
		</div>
	</div>
	<div class="columns-two">
        <div class="form-group">
			<input class="form-control" id="input-telefono-prepa" maxlength="10" name="telefono" placeholder="Teléfono*" required type="tel" pattern="[0-9-+]{6,10}" title="Por favor evitar letras o caracteres especiales o espacios en el teléfono." onkeydown="limit_prepa(this);" onkeyup="limit_prepa(this);"/> 
		</div>
		<div class="form-group">
			<input class="form-control" id="input-mail-prepa" maxlength="30" name="mail" placeholder="E-mail*" required type="email" /> 
		</div>		
	</div>
	<div class="form-group">
		<select class="form-control" id="input-plantel-prepa" name="plantel" required>
			<option value="">Plantel</option>
		</select>
		<script>

		</script>
	</div>		
	<input type="hidden" value="organico" name="utm_source" id="utm_source-prepa">
	<input type="hidden" value="organico" name="utm_medium" id="utm_medium-prepa">
	<input type="hidden" value="organico" name="utm_campaign" id="utm_campaign-prepa">
	<input type="hidden" value="organico" name="utm_content" id="utm_content-prepa">
	<input type="hidden" value="Prepa-LP" name="subOrigenCandidato" id="subOrigenCandidato-prepa">
	<input type="hidden" value="" name="website" id="website-prepa">
	<div class="form-check">
		<label>
			<input id="aceptar-prepa" required="true" type="checkbox" name="avisoPriv" /> He le&iacute;do el <a href="#elementor-action%3Aaction%3Dpopup%3Aopen%26settings%3DeyJpZCI6IjMxOTYiLCJ0b2dnbGUiOmZhbHNlfQ%3D%3D">Aviso de privacidad</a> 
		</label>
	</div>
	<button type="submit" class="btn btn-success btn-block enviar" id="enviar-prepa">Enviar</button>
	<div id="bacblack-prepa" class="bacblack" onClick="cerraraviso_prepa()"></div>
	<div id="resp-prepa" class="resp hidden">
		<h3 id="resp_cont_Title-prepa" class="resp_cont_Title"></h3>
		<p id="resp_cont-prepa" class="resp_cont"></p>
	</div>
</form>
<div id="response-prepa"></div>
<script>
	function limit_prepa(element){
    	var max_chars = 10;
    	if(element.value.length > max_chars) {
        	element.value = element.value.substr(0, max_chars);
    	}
	}
	const inptoken = document.getElementById("token-prepa");
	const bacblack_prepa = document.getElementById("bacblack-prepa");
	const resp_prepa = document.getElementById("resp-prepa");
	const resp_cont_prepa = document.getElementById("resp_cont-prepa");
	const resp_cont_Title_prepa = document.getElementById("resp_cont_Title-prepa");
	const formid_prepa = document.getElementById("form-prepa");
	const opPlantel_prepa = document.getElementById("input-plantel-prepa");
	const inp_source_prepa= document.getElementById("utm_source-prepa");
	const inp_medium_prepa= document.getElementById("utm_medium-prepa");
	const inp_campaign_prepa= document.getElementById("utm_campaign-prepa");
	const inp_content_prepa= document.getElementById("utm_content-prepa");
	const website_prepa= document.getElementById("website-prepa");
	const site_prepa = window.location.href;
	
	/*ESCUCHAR URL*/
	const queryString_prepa = window.location.search;
	const urlParams_prepa = new URLSearchParams(queryString_prepa);
	const utm_source_prepa = urlParams_prepa.get('utm_source');
	const utm_medium_prepa = urlParams_prepa.get('utm_medium');
	const utm_campaign_prepa = urlParams_prepa.get('utm_campaign');
	const utm_content_prepa = urlParams_prepa.get('utm_content');
	//opPlantel in
	function getPlantelPrepa() {
		website_prepa.value=site_prepa;
		inptoken.value = token;
		var xhttp = new XMLHttpRequest();						
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var PlantelPrepa = JSON.parse(this.responseText);
				//console.log (programa);
				for (var i = 0; i < PlantelPrepa.PlantelesPrepa.length; i++) {
					option = document.createElement("option");
					option.text = PlantelPrepa.PlantelesPrepa[i].idValue;
					option.value = PlantelPrepa.PlantelesPrepa[i].idPlantel;
					opPlantel_prepa.appendChild(option);
				};	
			}
		};
		xhttp.open("GET", "plantel-prepa.php?token="+token, true);
		xhttp.send();
		utms_prepa();
	}
	window.onload = getPlantelPrepa;
	//opPlantel out	
	function utms_prepa(){
		//INYECTAMOSVALORESDEURL
		if(utm_source_prepa != "" && utm_source_prepa != null){inp_source_prepa.value=utm_source_prepa;}
		if(utm_medium_prepa != "" && utm_medium_prepa != null){inp_medium_prepa.value=utm_mediu_prepam;}
		if(utm_campaign_prepa != "" && utm_campaign_prepa != null){inp_campaign_prepa.value=utm_campaign_prepa;}
		if(utm_content_prepa != "" && utm_content_prepa != null){inp_content_prepa.value=utm_content_prepa;}
	}
	
	//
    function cargarPrepa() {
		event.preventDefault();		
		if(jQuery("#form-prepa")[0].checkValidity()) {
			var formData = JSON.stringify(jQuery('#form-prepa').serializeArray());
			var ufrmData = "00-safe-form-prepa.php";
			const xhr = new XMLHttpRequest();
			// listen for `load` event
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {			
					// parse JSON
					let mensajelead = JSON.parse(xhr.responseText);
					//console.log(response);
					if(mensajelead.code==400){
						resp_cont_Title_prepa.innerHTML = "Lo sentimos.";
						resp_cont_prepa.innerHTML = "Tu solicitud no ha sido enviada, prueba nuevamente un unos minutos.";
						console.log(xhr.responseText);
					}
					
					if(mensajelead.idLead){
						var idLead=mensajelead.idLead;
						var tipo= "nuevo";
						console.log("nuevo"+idLead);
					}else if(mensajelead.idLeadExistente){
						var idLead=mensajelead.idLeadExistente;
						var tipo= "registrado";
							console.log("Duplicado"+idLead) 
					}
					
					if(mensajelead.Mensaje=='Guardado' || mensajelead.Mensaje=='Duplicados'){
						resp_cont_Title_prepa.innerHTML = "Gracias por contactarnos.";
						resp_cont_prepa.innerHTML = "Tu solicitud ha sido enviada y en unos momentos un asesor te contactará.";
						formid_prepa.reset();
					}
					   
				}else{
					var formHeight = formid_prepa.offsetHeight;
					document.getElementById("response-prepa").innerHTML = "Cargado...";
					bacblack_prepa.style.height = formHeight+"px";
					bacblack_prepa.classList.toggle("show");
					resp_prepa.classList.remove("hidden");
					resp_prepa.classList.add("show");
					resp_cont_Title_prepa.innerHTML = "Cargado...";
				}
				document.getElementById("response-prepa").innerHTML = xhr.responseText;
			};
			// open request
			xhr.open('POST', ufrmData, true);
			// set `Content-Type` header
			xhr.setRequestHeader('Content-Type', 'application/json');
			// send rquest with x-www payload
			xhr.send(formData);
    	}else{
			console.log("Faltan datos");
		}
	return false;
	};
	
	function cerraraviso_prepa (){
		bacblack_prepa.classList.toggle("show");
		resp_prepa.classList.remove("show");
		resp_prepa.classList.add("hidden");
		resp_cont_Title_prepa.innerHTML ="";
		resp_cont_prepa.innerHTML = "";
	}
</script>
</body>
</html>