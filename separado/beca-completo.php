<?php
//OBTENER BECA PROMEDIO INSCRIPCIÓN
$token = $_GET["token"];
$promedio = $_GET["promedio"];
$idOfertaEducativa = $_GET["idOfertaEducativa"];
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/getCatalogo',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "catalogo":"BecaCalculadora",
    "promedio" : '.$promedio.'
}',
  CURLOPT_HTTPHEADER => array(
    'Username: oscar.tapia@ieu.edu.mx',
    'Authorization: Bearer '.$token.'',
    'Content-Type: application/json',
    'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
  ),
));

$BecaCalculadora = curl_exec($curl);
//OBTENER BECAID MENSUALIDAD
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/getCatalogo',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "catalogo" : "Beca",
    "idOfertaEducativa" : "'.$idOfertaEducativa.'"
}',
  CURLOPT_HTTPHEADER => array(
    'Username: oscar.tapia@ieu.edu.mx',
    'Authorization: Bearer '.$token.'',
    'Content-Type: application/json',
    'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
  ),
));

$Beca = curl_exec($curl);

curl_close($curl);

$jBecaCalculadora = json_decode($BecaCalculadora, true);
$jBeca = json_decode($Beca, true);
$ArrayRespuesta = array_merge($jBecaCalculadora,$jBeca);
$respuesta = json_encode($ArrayRespuesta);
print_r($respuesta);

?>