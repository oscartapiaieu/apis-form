<?php
$token = $_GET["token"];
$idUbicacionEstado =$_GET["idestado"];
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ieu--dev03.my.salesforce.com/services/apexrest/getCatalogo',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
 "catalogo" : "Municipio",
 "idUbicacionEstado" : "'.$idUbicacionEstado.'"
}',
  CURLOPT_HTTPHEADER => array(
    'Username: oscar.tapia@ieu.edu.mx',
    'Authorization: Bearer '.$token.'',
    'Content-Type: application/json',
    'Cookie: BrowserId=GVIROZZUEeujWbVpZyZnbA'
  ),
));

$municipio = curl_exec($curl);

curl_close($curl);
echo $municipio;
?>